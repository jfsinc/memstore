package main

import (
	"bufio"
	"flag"
	"io"
	"log"
	"memstore/app/common"
	"net"
)

var clientId = flag.String("id", "anonymous", "client identifier")
var host = flag.String("host", "localhost", "host name of server")
var port = flag.String("port", "8092", "tcp port of server")
var action = flag.String("action", "", "database action to perform [add,get,set,remove]")
var key = flag.String("key", "", "key to use when performing action")
var value = flag.String("value", "", "value to store when calling add,get")

// Message line terminator.
const delim byte = '\n'
const endl string = "\n"

func main() {
	flag.Parse()

	conn, err := connect(*host, *port)
	if err != nil {
		log.Fatal(err)
	}

	// Build payload.
	packet := common.Packet{}
	packet.Action = *action
	packet.Key = *key
	packet.Value = *value
	data, err := packet.Encode()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Sending packet", data)

	// Send payload.
	resp, err := transmit(conn, data)
	if err != nil {
		if err == io.EOF {
			log.Fatal("Server disconnect")
		} else {
			log.Fatal(err)
		}
	}

	log.Println("Response from server", resp)
}

func connect(host, port string) (net.Conn, error) {
	return net.Dial("tcp", host+":"+port)
}

func write(conn net.Conn, message string) error {
	_, err := conn.Write([]byte(message + endl))
	return err
}

func read(conn net.Conn) (string, error) {
	return bufio.NewReader(conn).ReadString(delim)
}

func transmit(conn net.Conn, message string) (string, error) {
	var response string
	if err := write(conn, message); err != nil {
		return response, err
	}
	response, err := read(conn)
	return response, err
}
