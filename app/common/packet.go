package common

import (
	"encoding/json"
)

const (
	Add    = "add"
	Get    = "get"
	Set    = "set"
	Remove = "remove"
)

// Packet is a common transport for memstore client/server actions.
type Packet struct {
	Action string
	Key    string
	Value  string
}

// Encode JSON-encodes packet and returns the string representation.
func (p Packet) Encode() (string, error) {
	data, err := json.Marshal(p)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

// Decode JSON-decodes packet into itself.
func (p *Packet) Decode(data string) error {
	if err := json.Unmarshal([]byte(data), &p); err != nil {
		return err
	}
	return nil
}
