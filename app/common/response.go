package common

import "encoding/json"

// Response codes.
const (
	Ok    = 1
	Error = 2
)

// Response represents a server response to a client action.
type Response struct {
	Code    int
	Message string
}

// Encode JSON-encodes response and returns the string representation.
func (r Response) Encode() (string, error) {
	data, err := json.Marshal(r)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

// Decode JSON-decodes response into itself.
func (r *Response) Decode(data string) error {
	if err := json.Unmarshal([]byte(data), &r); err != nil {
		return err
	}
	return nil
}
