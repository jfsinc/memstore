package main

import (
	"bufio"
	"flag"
	"io"
	"log"
	"memstore/app/common"
	"memstore/mmdb"
	"net"
)

var port = flag.String("port", "8092", "tcp port the server sends/receives on")

// Message line terminator.
const delim byte = '\n'
const endl string = "\n"

// Database store.
var db *mmdb.Db

func main() {
	flag.Parse()

	ln, err := net.Listen("tcp", ":"+*port)
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()

	log.Println("Server started")
	log.Println("Listen on port " + *port)

	// Initialize data store.
	db = mmdb.New()

	for {
		// Wait for a connection.
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal(err)
		}
		// Let the loop continue to accept multiple concurrent connections.
		go func(c net.Conn) {
			for {
				// Parse client request.
				var packet common.Packet
				data, err := bufio.NewReader(conn).ReadString(delim)
				log.Println("Received data", data)
				if err != nil {
					if err == io.EOF {
						log.Println("Client disconnect")
					} else {
						log.Println("Error communicating with client: ", err)
					}
					break
				}

				// Decode packet.
				if err := packet.Decode(data); err != nil {
					log.Fatal(err, data)
				}

				// Do action.
				response := do(packet)
				log.Println(response)
				// Send response.
				resp, err := response.Encode()
				if err != nil {
					log.Fatal(err)
				}

				c.Write([]byte(resp + endl))
			}
			// Shut down the connection.
			c.Close()
		}(conn)
	}
}

func do(packet common.Packet) common.Response {
	response := common.Response{Code: common.Ok}

	switch packet.Action {
	case common.Add:
		if err := db.Add(packet.Key, packet.Value); err != nil {
			response.Code = common.Error
			response.Message = err.Error()
			return response
		}
	case common.Get:
		var i string
		err := db.Get(packet.Key, &i)
		if err != nil {
			response.Code = common.Error
			response.Message = err.Error()
			return response
		}
		response.Message = i
	case common.Set:
		if err := db.Set(packet.Key, packet.Value); err != nil {
			response.Code = common.Error
			response.Message = err.Error()
			return response
		}
	case common.Remove:
		if err := db.Remove(packet.Key); err != nil {
			response.Code = common.Error
			response.Message = err.Error()
			return response
		}
	case "":
		response.Code = common.Error
		response.Message = "Action not supplied"
		return response
	default:
		response.Code = common.Error
		response.Message = "Unsupported action in packet: " + packet.Action
		return response
	}

	return response
}
