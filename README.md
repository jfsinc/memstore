# Overview #

Memstore is a simple client/server interface for storing encoded data in machine memory. The server runs as a standalone process and listens for client connections on the port supplied via a command line argument. The server supports any number of concurrent client connections for sending and receiving of data packets.

# Setup #

go get bitbucket.org/jfsinc/memstore

go install

go test

# Using the Server #

A basic server is provided in app/memstoreserver. After install/build, run the binary with the following options:


./memstoreserver -port 8092


The port flag is optional and defaults to port 8092.

# Using the Client #

A basic client is provided in app/memstoreclient. After install/build, run the binary with the following options:


./memstoreclient -id myclient -host localhost -port 8092 -action set -key mykey -value myvalue


 valid actions are add, get, set, remove

# Request & Response Format #
All client/server packets are JSON-encoded.