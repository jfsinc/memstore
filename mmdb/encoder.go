package mmdb

type Encoder interface {
	Encode(interface{}) ([]byte, error)
	Decode([]byte, interface{}) (interface{}, error)
}
