package mmdb

import "encoding/json"

type JsonEncoder struct{}

// Encode JSON encodes supplied interface and returns bytes.
func (e JsonEncoder) Encode(i interface{}) ([]byte, error) {
	return json.Marshal(i)
}

// Decode JSON decodes supplied bytes into interface and returns it.
func (e JsonEncoder) Decode(b []byte, i interface{}) error {
	return json.Unmarshal(b, &i)
}
