package mmdb

import (
	"gostruts/simpledb"
)

type Db struct {
	controller simpledb.Controller
	encoder    JsonEncoder
}

// New returns a db
func New() *Db {
	return &Db{
		controller: simpledb.New(),
		encoder:    JsonEncoder{},
	}
}

// Add encodes, packs, and stores value using its controller.
func (d *Db) Add(key string, value interface{}) error {
	bytes, err := d.encoder.Encode(value)
	if err != nil {
		return err
	}

	return d.controller.GetContainer().Add(key, bytes)
}

// Get retrieves a value from its controller and unpacks, decodes into supplied interface.
func (d Db) Get(key string, i interface{}) error {
	data, err := d.controller.GetContainer().Get(key)
	if err != nil {
		return err
	}

	if err := d.encoder.Decode(data.([]byte), i); err != nil {
		return err
	}

	return nil
}

// Remove removes a key/value via controller.
func (d *Db) Remove(key string) error {
	return d.controller.GetContainer().Remove(key)
}

// Set sets a key/value to container.
// If value exists for key, it will be overwritten.
func (d *Db) Set(key string, value interface{}) error {
	bytes, err := d.encoder.Encode(value)
	if err != nil {
		return err
	}

	return d.controller.GetContainer().Set(key, bytes)
}
