package mmdb

import (
	. "gopkg.in/check.v1"
)

type DbSuite struct {
	db *Db
}

var _ = Suite(&DbSuite{})

func (s *DbSuite) SetUpTest(c *C) {
	s.db = New()
}

func (s *DbSuite) TestAdd(c *C) {
	c.Assert(s.db.Add("key", "value"), IsNil)
}

func (s *DbSuite) TestDoubleAdd(c *C) {
	key := "key"
	value := "value"
	c.Assert(s.db.Add(key, value), IsNil)
	c.Assert(s.db.Add(key, value), ErrorMatches, "Key exists.")
}

func (s *DbSuite) TestAddGet(c *C) {
	key := "key"
	value := "value"
	var read string
	c.Assert(s.db.Add(key, value), IsNil)
	err := s.db.Get(key, &read)
	c.Assert(err, IsNil)
	c.Assert(read, Equals, value)
}

func (s *DbSuite) TestRemoveNotExists(c *C) {
	c.Assert(s.db.Remove("key"), ErrorMatches, "Key does not exist.")
}

func (s *DbSuite) TestAddRemoveGet(c *C) {
	key := "key"
	value := "value"
	c.Assert(s.db.Add(key, value), IsNil)
	c.Assert(s.db.Remove(key), IsNil)
	var read string
	err := s.db.Get(key, &read)
	c.Assert(err, ErrorMatches, "Key does not exist.")
}

func (s *DbSuite) TestSetGet(c *C) {
	key := "key"
	value := "value"
	s.db.Set(key, value)
	var read string
	err := s.db.Get(key, &read)
	c.Assert(err, IsNil)
	c.Assert(read, Equals, value)
	c.Assert(read, Equals, value)
}

func (s *DbSuite) TestDoubleSetGet(c *C) {
	key := "key"
	value := "value"
	value2 := "value2"
	s.db.Set(key, value)
	s.db.Set(key, value2)
	var read string
	c.Assert(s.db.Get(key, &read), IsNil)
	c.Assert(read, Equals, value2)
}

func (s *DbSuite) TestAddSet(c *C) {
	key := "key"
	value := "value"
	value2 := "value2"
	c.Assert(s.db.Add(key, value), IsNil)
	s.db.Set(key, value2)
	var read string
	c.Assert(s.db.Get(key, &read), IsNil)
	c.Assert(read, Equals, value2)
}

func (s *DbSuite) TestAddDoubleRemove(c *C) {
	c.Assert(s.db.Add("key", "value"), IsNil)
	c.Assert(s.db.Remove("key"), IsNil)
	c.Assert(s.db.Remove("key"), ErrorMatches, "Key does not exist.")
}

func (s *DbSuite) TestAddResetGet(c *C) {
	key := "key"
	value := "value"
	c.Assert(s.db.Add(key, value), IsNil)
	c.Assert(s.db.controller.Reset(), IsNil)

	var read string
	c.Assert(s.db.Get(key, &read), ErrorMatches, "Key does not exist.")
	c.Assert(read, Equals, "")
}
