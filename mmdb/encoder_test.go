package mmdb

import (
	. "gopkg.in/check.v1"
)

type JsonEncoderSuite struct{}

var _ = Suite(&JsonEncoderSuite{})

func (s *JsonEncoderSuite) TestPackageEncodePackUnpackDecode(c *C) {
	type data struct {
		A string
		B []int
	}
	d := data{"A", []int{1, 2, 3}}

	// Encode
	e := JsonEncoder{}
	encoded, err := e.Encode(d)
	c.Assert(err, IsNil)

	// Decode
	var newData data
	c.Assert(e.Decode(encoded, &newData), IsNil)
	c.Assert(newData, DeepEquals, d)
	c.Assert(newData.A, DeepEquals, d.A)
	c.Assert(newData.B, DeepEquals, d.B)
}
